package com.praksa.api;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ApiServiceTest {


    @Test
    public void evenNumTest()
    {
        List<Integer> l2 = new ArrayList<Integer>();
        l2.add(1);
        l2.add(2);
        l2.add(3);
        l2.add(4);
        l2.add(5);
        int result = ApiService.evenNum(l2);
        assertEquals(2,result);

    }

    @Test
    public void testSort()
    {
        List<String> unsorted = new ArrayList<String>();
        unsorted.add("Marko");
        unsorted.add("Luka");
        unsorted.add("Ivan");
        unsorted.add("Petar");
        unsorted.add("Josip");

        unsorted = ApiService.sort(unsorted);
        List<String> sorted = new ArrayList<String>();
        sorted.add("Ivan");
        sorted.add("Josip");
        sorted.add("Luka");
        sorted.add("Marko");
        sorted.add("Petar");
        assertEquals(unsorted,sorted);
    }

    @Test
    public void charactersCountTest(){
        //lista stringova
        List<String> strings = new ArrayList<String>();
        strings.add("Marko");
        strings.add("Luka");
        strings.add("Ivan");
        strings.add("Petar");
        strings.add("Josip");

        Map stringsMap = new HashMap();
        stringsMap = ApiService.charactersCount(strings);//pozivamo metodu i rez pohranjujemo u StringsMap

        Map realMap = new HashMap();//Mapa sa kojom uspoređujemo
        realMap.put("Marko",5);
        realMap.put("Luka",4);
        realMap.put("Ivan",4);
        realMap.put("Petar",5);
        realMap.put("Josip",5);

        assertEquals(stringsMap,realMap);

    }
}
