package com.praksa.student;


import com.praksa.api.ApiService;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class studentServiceTest {
   public  List<Student> studentListst = new ArrayList<>();
    public Student student = new Student();
    public Student student2 = new Student();
    public Student student3 = new Student(); //globalne varijable

   public void popisStudenata()
   {

       student.setIndex(50);
       student.setName("Marko");
       student.setEmail("markomaric@gmail.com");


       student2.setName("Petar");
       student2.setIndex(4);
       student2.setEmail("as<rfa333"); //dogodit ce se  Exception


       student3.setName("Luka");
       student3.setEmail("lukalukic@gmail.com");
       student3.setIndex(3);

       studentListst.add(student);
       studentListst.add(student2);
       studentListst.add(student3);

   }

    @Test
    public void student() throws Exception {
        popisStudenata();  //poziv popisa studenata
        StudentService.isEmailValid(studentListst);
    }

    @Test
    public void indexCheck(){
        Map realMap = new HashMap();//Mapa sa kojom uspoređujemo
        realMap.put("Marko","True");
        realMap.put("Petar","True");
        realMap.put("Luka","True");
        Map newMap;
        popisStudenata();   //poziv popisa studenata
        newMap = StudentService.checkIndex(studentListst);
        assertEquals(realMap, newMap );

    }
}


