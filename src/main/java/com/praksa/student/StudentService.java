package com.praksa.student;

import org.apache.commons.validator.routines.EmailValidator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StudentService {

    public static boolean isEmailValid(List<Student> emails) throws myException {
        for (Student pomEmail : emails) {
            if(pomEmail.getEmail().matches("^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$")) {
                continue;
            } else {
                throw new myException("Invalid Email");
            }
        }
        return true;
    }

    public static Map<String, String> checkIndex(List<Student> students) {
        Map<String, String> newMap = new HashMap<String, String>();
        for (Student student : students) {
            if (student.getIndex() < 100) {
                newMap.put(student.getName(), "True");
            } else
            {
                newMap.put(student.getName(), "False");
            }
        }
        return newMap;
    }
}
