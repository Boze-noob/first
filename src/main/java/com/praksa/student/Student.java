package com.praksa.student;

public class Student {
    int indexId;
    String name;
    String email;

    public void setIndex(int newIndex) {
        this.indexId = newIndex;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public void setEmail(String newEmail) {
        this.email = newEmail;
    }

    public int getIndex() {
        return indexId;
    }

    public String getName() {
        return name;

    }

    public String getEmail() {
        return email;
    }
}
