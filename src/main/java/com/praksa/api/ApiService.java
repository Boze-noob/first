package com.praksa.api;

import java.util.*;

public class ApiService {
    public static int evenNum(List<Integer> brojevi)
    {
        int num=0;
        for(int number : brojevi) {
            if(number % 2 == 0) {
                num++;
            }
        }
        for(int i = 0;i<brojevi.size();i++)
        {
           if(brojevi.get(i)%2 == 0)
           {
               num++;
           }
        }
        return num;
    }

    public static List<String> sort(List<String> str)
    {
        Collections.sort(str);
        return str;
    }

    //TODO: Napraviti metodu koja prima Listu Stringova i vraća Mapu sa key String, a value broj koliko ima slova u stringu

public static Map<String,Integer> charactersCount (List<String> words){
    Map stringsmap = new HashMap();
    for(String helpStr : words)     //foreach loop
    {
     stringsmap.put(helpStr,helpStr.length()); //upis key i value vrijednosti u mapu
    }
return stringsmap;
}

}
